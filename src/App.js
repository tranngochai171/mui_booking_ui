import Header from "./components/Header";
import Navbar from "./components/Navbar";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Hotel from "./pages/hotel/Hotel";
import Home from "./pages/home/Home";
import List from "./pages/list/List";
function App() {
	return (
		<BrowserRouter>
			<Routes>
				<Route path="/" element={<Home />}></Route>
				<Route path="/hotels" element={<List />}></Route>
				<Route path="/hotel" element={<Hotel />}></Route>
			</Routes>
		</BrowserRouter>
	);
}

export default App;
