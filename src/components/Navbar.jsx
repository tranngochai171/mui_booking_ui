import { styled, Box, Stack, Button, Typography } from "@mui/material";

const NavBarContainer = styled(Box)({
	height: "72px",
	backgroundColor: "#003580",
	display: "flex",
	justifyContent: "center",
});

const NavbarWrapper = styled(Stack)({
	width: "100%",
	maxWidth: "1024px",
	alignItems: "center",
	justifyContent: "space-between",
});

const Logo = styled(Typography)({
	color: "#fff",
});

const StyledButton = styled(Button)({
	color: "#003580",
	backgroundColor: "#fff",
	fontWeight: "bold",
	padding: "5px 14px",
	textTransform: "unset",
	"&:hover": {
		color: "#003580",
		backgroundColor: "#e0dede",
	},
});

const Navbar = () => {
	return (
		<NavBarContainer>
			<NavbarWrapper direction="row">
				<Logo variant="h6" fontWeight="800">
					Topybooking
				</Logo>
				<Stack direction="row" gap={2}>
					<StyledButton>Register</StyledButton>
					<StyledButton>Login</StyledButton>
				</Stack>
			</NavbarWrapper>
		</NavBarContainer>
	);
};

export default Navbar;
