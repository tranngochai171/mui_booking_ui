import { Button, Stack, styled, Typography } from "@mui/material";

const FPContainer = styled(Stack)({
	flexDirection: "row",
	justifyContent: "space-between",
	width: "100%",
	maxWidth: "1024px",
	gap: "10px",
});

const FPItem = styled(Stack)({
	flex: 1,
	flexDirection: "column",
});

const FPItemImg = styled("img")({
	width: "100%",
	height: "240px",
	objectFit: "cover",
	borderRadius: "10px",
});

const PTItemName = styled(Typography)({
	fontSize: "14px",
	fontWeight: "400",
	color: "#6b6b6b",
});
const PTItemCity = styled(Typography)({
	fontSize: "14px",
	fontWeight: "400",
	color: "#6b6b6b",
});
const PTItemPrice = styled(Typography)({
	fontSize: "14px",
	fontWeight: "600",
	lineHeight: "20px",
	margin: "8px 0 4px 0",
});
const PTRating = styled(Stack)({
	flexDirection: "row",
	gap: 8,
});
const PTRatingButton = styled(Button)({
	padding: "1px",
	backgroundColor: "#003580",
	color: "#fff",
	minWidth: "38px",
	"&:hover": {
		backgroundColor: "#003580",
		color: "#fff",
	},
});

const FeaturedProperties = () => {
	return (
		<FPContainer>
			<FPItem>
				<FPItemImg
					src="https://cf.bstatic.com/xdata/images/hotel/square600/13125860.webp?k=e148feeb802ac3d28d1391dad9e4cf1e12d9231f897d0b53ca067bde8a9d3355&o=&s=1"
					alt=""
				/>
				<PTItemName>Aparthotel Stare Miasto</PTItemName>
				<PTItemCity>Madrid</PTItemCity>
				<PTItemPrice>Starting from $120</PTItemPrice>
				<PTRating>
					<PTRatingButton>8.9</PTRatingButton>
					<Typography
						variant="subtitle1"
						component="span"
						fontWeight="600"
						color="#262626"
					>
						Excellent
					</Typography>
				</PTRating>
			</FPItem>
			<FPItem>
				<FPItemImg
					src="https://cf.bstatic.com/xdata/images/hotel/max1280x900/215955381.jpg?k=ff739d1d9e0c8e233f78ee3ced82743ef0355e925df8db7135d83b55a00ca07a&o=&hp=1"
					alt=""
				/>
				<PTItemName>Comfort Suites Airport</PTItemName>
				<PTItemCity>Austin</PTItemCity>
				<PTItemPrice>Starting from $140</PTItemPrice>
				<PTRating>
					<PTRatingButton>9.3</PTRatingButton>
					<Typography
						variant="subtitle1"
						component="span"
						fontWeight="600"
						color="#262626"
					>
						Exceptional
					</Typography>
				</PTRating>
			</FPItem>
			<FPItem>
				<FPItemImg
					src="https://cf.bstatic.com/xdata/images/hotel/max1280x900/232902339.jpg?k=3947def526b8af0429568b44f9716e79667d640842c48de5e66fd2a8b776accd&o=&hp=1"
					alt=""
				/>
				<PTItemName>our Seasons Hotel</PTItemName>
				<PTItemCity>Lisbon</PTItemCity>
				<PTItemPrice>Starting from $99</PTItemPrice>
				<PTRating>
					<PTRatingButton>8.8</PTRatingButton>
					<Typography
						variant="subtitle1"
						component="span"
						fontWeight="600"
						color="#262626"
					>
						Excellent
					</Typography>
				</PTRating>
			</FPItem>
			<FPItem>
				<FPItemImg
					src="https://cf.bstatic.com/xdata/images/hotel/max1280x900/322658536.jpg?k=3fffe63a365fd0ccdc59210188e55188cdb7448b9ec1ddb71b0843172138ec07&o=&hp=1"
					alt=""
				/>
				<PTItemName>Hilton Garden Inn</PTItemName>
				<PTItemCity>Berlin</PTItemCity>
				<PTItemPrice>Starting from $105</PTItemPrice>
				<PTRating>
					<PTRatingButton>8.9</PTRatingButton>
					<Typography
						variant="subtitle1"
						component="span"
						fontWeight="600"
						color="#262626"
					>
						Excellent
					</Typography>
				</PTRating>
			</FPItem>
		</FPContainer>
	);
};

export default FeaturedProperties;
