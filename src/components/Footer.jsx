import { Stack, styled, Typography } from "@mui/material";

const FooterContainer = styled(Stack)({
	width: "100%",
	maxWidth: "1024px",
	fontSize: "12px",
	gap: "50px",
});

const FooterLists = styled(Stack)({
	flexDirection: "row",
	justifyContent: "space-between",
	gap: "10px",
});
const FooterList = styled(Stack)({
	gap: "4px",
});
const FooterListItem = styled(Typography)({
	fontSize: "12px",
	color: "#003580",
	cursor: "pointer",
});

const FooterCopyRight = styled(Typography)({
	fontSize: "11.2px",
});

const Footer = () => {
	return (
		<FooterContainer>
			<FooterLists>
				<FooterList>
					<FooterListItem>Countries</FooterListItem>
					<FooterListItem>Regions</FooterListItem>
					<FooterListItem>Cities</FooterListItem>
					<FooterListItem>District</FooterListItem>
					<FooterListItem>Airports</FooterListItem>
					<FooterListItem>Hotels</FooterListItem>
				</FooterList>
				<FooterList>
					<FooterListItem>Countries</FooterListItem>
					<FooterListItem>Regions</FooterListItem>
					<FooterListItem>Cities</FooterListItem>
					<FooterListItem>District</FooterListItem>
					<FooterListItem>Airports</FooterListItem>
					<FooterListItem>Hotels</FooterListItem>
				</FooterList>
				<FooterList>
					<FooterListItem>Countries</FooterListItem>
					<FooterListItem>Regions</FooterListItem>
					<FooterListItem>Cities</FooterListItem>
					<FooterListItem>District</FooterListItem>
					<FooterListItem>Airports</FooterListItem>
					<FooterListItem>Hotels</FooterListItem>
				</FooterList>
				<FooterList>
					<FooterListItem>Countries</FooterListItem>
					<FooterListItem>Regions</FooterListItem>
					<FooterListItem>Cities</FooterListItem>
					<FooterListItem>District</FooterListItem>
					<FooterListItem>Airports</FooterListItem>
					<FooterListItem>Hotels</FooterListItem>
				</FooterList>
				<FooterList>
					<FooterListItem>Countries</FooterListItem>
					<FooterListItem>Regions</FooterListItem>
					<FooterListItem>Cities</FooterListItem>
					<FooterListItem>District</FooterListItem>
					<FooterListItem>Airports</FooterListItem>
					<FooterListItem>Hotels</FooterListItem>
				</FooterList>
			</FooterLists>
			<FooterCopyRight>
				Copyright © 1996–2022 TopyBooking.com™. All rights reserved.
			</FooterCopyRight>
		</FooterContainer>
	);
};

export default Footer;
