import { Button, Stack, styled, Typography, InputBase } from "@mui/material";

const MailContainer = styled(Stack)({
	width: "100%",
	backgroundColor: "#003580",
	color: "#fff",
	flexDirection: "column",
	alignItems: "center",
	padding: "50px",
	gap: "20px",
});

const MailTitle = styled(Typography)({
	fontSize: "24px",
});

const MailDesc = styled(Typography)({});

const MailInputContainer = styled(Stack)({
	alignItems: "center",
	flexDirection: "row",
	gap: "10px",
});

const MainInput = styled(InputBase)({
	color: "#003580",
	backgroundColor: "#fff",
	width: "300px",
	height: "40px",
	padding: "14px",
	borderRadius: "10px",
});

const MailButton = styled(Button)({
	textTransform: "unset",
	borderRadius: "10px",
	height: "40px",
});

const MailList = () => {
	return (
		<MailContainer>
			<MailTitle>Save time, save money!</MailTitle>
			<MailDesc>Sign up and we'll send the best deals to you</MailDesc>
			<MailInputContainer>
				<MainInput placeholder="Your email" />
				<MailButton variant="contained">Subscribe</MailButton>
			</MailInputContainer>
		</MailContainer>
	);
};

export default MailList;
