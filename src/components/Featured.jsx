import { Box, Stack, styled, Typography } from "@mui/material";

const FeaturedContainer = styled(Stack)({
	width: "100%",
	maxWidth: "1024px",
	flexDirection: "row",
	gap: "10px",
	zIndex: "1",
	justifyContent: "space-between",
});

const FeaturedItem = styled(Box)({
	borderRadius: "10px",
	overflow: "hidden",
	height: "250px",
	position: "relative",
	flex: 1,
	color: "#fff",
});

const FeaturedImg = styled("img")({
	width: "100%",
	objectFit: "cover",
});

const FeaturedTitles = styled(Stack)({
	gap: 2,
	position: "absolute",
	top: "20px",
	left: "20px",
});

const Title = styled(Typography)({
	fontSize: "30px",
	fontWeight: "bold",
});

const Subtitle = styled(Typography)({
	fontSize: "24px",
	fontWeight: "bold",
});

const Featured = () => {
	return (
		<FeaturedContainer>
			<FeaturedItem>
				<FeaturedImg src="https://cf.bstatic.com/xdata/images/city/max500/957801.webp?k=a969e39bcd40cdcc21786ba92826063e3cb09bf307bcfeac2aa392b838e9b7a5&o=" />
				<FeaturedTitles>
					<Title>Dublin</Title>
					<Subtitle>123 properties</Subtitle>
				</FeaturedTitles>
			</FeaturedItem>
			<FeaturedItem>
				<FeaturedImg src="https://cf.bstatic.com/xdata/images/city/max500/690334.webp?k=b99df435f06a15a1568ddd5f55d239507c0156985577681ab91274f917af6dbb&o=" />
				<FeaturedTitles>
					<Title>Dublin</Title>
					<Subtitle>123 properties</Subtitle>
				</FeaturedTitles>
			</FeaturedItem>
			<FeaturedItem>
				<FeaturedImg src="https://cf.bstatic.com/xdata/images/city/max500/689422.webp?k=2595c93e7e067b9ba95f90713f80ba6e5fa88a66e6e55600bd27a5128808fdf2&o=" />
				<FeaturedTitles>
					<Title variant="h1">Dublin</Title>
					<Subtitle variant="h2">123 properties</Subtitle>
				</FeaturedTitles>
			</FeaturedItem>
		</FeaturedContainer>
	);
};

export default Featured;
