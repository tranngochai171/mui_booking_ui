import { Stack, styled, Typography } from "@mui/material";

const PropertyListContainer = styled(Stack)({
	width: "100%",
	maxWidth: "1024px",
	flexDirection: "row",
	justifyContent: "space-between",
	gap: "10px",
});

const PropertyListItem = styled(Stack)({
	flex: 1,
	borderRadius: "10px",
	overflow: "hidden",
	gap: "5px",
});
const PropertyListItemImg = styled("img")({
	width: "100%",
	height: "150px",
	objectFit: "cover",
	borderRadius: "10px",
});

const PropertyListItemTitles = styled(Stack)({});

const Title = styled(Typography)({
	fontSize: "18px",
	fontWeight: "bold",
});

const SubTitle = styled(Typography)({ fontSize: "15px" });

const PropertyList = () => {
	return (
		<PropertyListContainer>
			<PropertyListItem>
				<PropertyListItemImg
					src="https://cf.bstatic.com/xdata/images/xphoto/square300/57584488.webp?k=bf724e4e9b9b75480bbe7fc675460a089ba6414fe4693b83ea3fdd8e938832a6&o="
					alt=""
				/>
				<PropertyListItemTitles>
					<Title>Hotels</Title>
					<SubTitle>233 hotels</SubTitle>
				</PropertyListItemTitles>
			</PropertyListItem>
			<PropertyListItem>
				<PropertyListItemImg
					src="https://cf.bstatic.com/static/img/theme-index/carousel_320x240/card-image-apartments_300/9f60235dc09a3ac3f0a93adbc901c61ecd1ce72e.jpg"
					alt=""
				/>
				<PropertyListItemTitles>
					<Title>Apartments</Title>
					<SubTitle>2331 hotels</SubTitle>
				</PropertyListItemTitles>
			</PropertyListItem>
			<PropertyListItem>
				<PropertyListItemImg
					src="https://cf.bstatic.com/static/img/theme-index/carousel_320x240/bg_resorts/6f87c6143fbd51a0bb5d15ca3b9cf84211ab0884.jpg"
					alt=""
				/>
				<PropertyListItemTitles>
					<Title>Resorts</Title>
					<SubTitle>2331 hotels</SubTitle>
				</PropertyListItemTitles>
			</PropertyListItem>
			<PropertyListItem>
				<PropertyListItemImg
					src="https://cf.bstatic.com/static/img/theme-index/carousel_320x240/card-image-villas_300/dd0d7f8202676306a661aa4f0cf1ffab31286211.jpg"
					alt=""
				/>
				<PropertyListItemTitles>
					<Title>Villas</Title>
					<SubTitle>2331 hotels</SubTitle>
				</PropertyListItemTitles>
			</PropertyListItem>
			<PropertyListItem>
				<PropertyListItemImg
					src="https://cf.bstatic.com/static/img/theme-index/carousel_320x240/card-image-chalet_300/8ee014fcc493cb3334e25893a1dee8c6d36ed0ba.jpg"
					alt=""
				/>
				<PropertyListItemTitles>
					<Title>Cabins</Title>
					<SubTitle>2331 hotels</SubTitle>
				</PropertyListItemTitles>
			</PropertyListItem>
		</PropertyListContainer>
	);
};

export default PropertyList;
