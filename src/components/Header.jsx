import {
	styled,
	Box,
	Stack,
	Typography,
	Button,
	InputBase,
	Menu,
} from "@mui/material";
import { DateRange } from "react-date-range";
import { useState, useRef } from "react";
import { format } from "date-fns";
import { useImmer } from "use-immer";
import { useNavigate } from "react-router-dom";
import HotelIcon from "@mui/icons-material/Hotel";
import FlightIcon from "@mui/icons-material/Flight";
import DirectionsCarIcon from "@mui/icons-material/DirectionsCar";
import LocalTaxiIcon from "@mui/icons-material/LocalTaxi";
import CalendarMonthIcon from "@mui/icons-material/CalendarMonth";
import BoyIcon from "@mui/icons-material/Boy";
import "react-date-range/dist/styles.css"; // main css file
import "react-date-range/dist/theme/default.css"; // theme css file
import styles from "./Header.module.css";
import useOnClickOutside from "../hooks/useOnClickOutside";

const HeaderContainer = styled(Box)({
	backgroundColor: "#003580",
	display: "flex",
	justifyContent: "center",
	color: "#fff",
	position: "relative",
});

const HeaderWrapper = styled(Stack)(({ showOptions = true }) => ({
	backgroundColor: "#003580",
	width: "100%",
	maxWidth: "1024px",
	margin: showOptions ? "20px 0 60px 0" : "20px 0 40px 0",
}));

const HeaderList = styled(Stack)({});

const HeaderListItem = styled(Stack)(({ active }) => {
	let style = { padding: "10px" };
	if (active) {
		style = { ...style, border: "1px solid #fff", borderRadius: "20px" };
	}
	return style;
});

const HeaderButton = styled(Button)({
	backgroundColor: "#0071c2",
	color: "#fff",
	padding: "5px",
	textTransform: "unset",
	width: "150px",
});

const HeaderSearch = styled(Stack)({
	height: "30px",
	backgroundColor: "#fff",
	border: "3px solid #febb02",
	alignItems: "center",
	justifyContent: "space-around",
	color: "#333",
	padding: "10px 0",
	borderRadius: "5px",
	position: "absolute",
	bottom: "-25px",
	width: "100%",
	maxWidth: "1024px",
});

const HeaderSearchItem = styled(Stack)({
	gap: "10px",
	alignItems: "center",
});

const ListOption = styled(Stack)({
	width: "370px",
	padding: "20px 15px",
});

const ListOptionItem = styled(Stack)({
	justifyContent: "space-between",
	flexDirection: "row",
	alignItems: "center",
});

const ListOptionItemAction = styled(Stack)({
	justifyContent: "center",
	flexDirection: "row",
	alignItems: "center",
	gap: "10px",
});

const TYPE = {
	INCREASE: "INCREASE",
	DECREASE: "DECREASE",
};

const Header = ({ showOptions = true }) => {
	const dateRef = useRef();
	const [destination, setDestination] = useState("");
	const [openDate, setOpenDate] = useState(false);
	const [anchorEl, setAnchorEl] = useState(null);
	const [options, setOptions] = useImmer({
		adults: 1,
		children: 0,
		rooms: 1,
	});
	const [date, setDate] = useState([
		{
			startDate: new Date(),
			endDate: new Date(),
			key: "selection",
		},
	]);
	const navigate = useNavigate();
	const handleSearch = () => {
		navigate("/hotels", {
			state: {
				date,
				destination,
				options,
			},
		});
	};

	const handleOptionsChange = (option, type) => {
		setOptions((draft) => {
			draft[option] =
				type === TYPE.INCREASE ? draft[option] + 1 : draft[option] - 1;
		});
	};
	const handleOpenOptions = (event) => {
		setAnchorEl(event.currentTarget);
	};
	useOnClickOutside(dateRef, () => setOpenDate(false));

	return (
		<HeaderContainer>
			<HeaderWrapper gap={4}>
				<HeaderList direction="row" gap={5}>
					<HeaderListItem direction="row" gap={1} active="true">
						<HotelIcon />
						<Typography>Stays</Typography>
					</HeaderListItem>
					<HeaderListItem direction="row" gap={1}>
						<FlightIcon />
						<Typography>Flights</Typography>
					</HeaderListItem>
					<HeaderListItem direction="row" gap={1}>
						<DirectionsCarIcon />
						<Typography>Car rentals</Typography>
					</HeaderListItem>
					<HeaderListItem direction="row" gap={1}>
						<LocalTaxiIcon />
						<Typography>Airport taxis</Typography>
					</HeaderListItem>
				</HeaderList>
				{showOptions && (
					<>
						<Stack gap={1}>
							<Typography variant="h1" sx={{ fontSize: "4rem" }}>
								A lifetime of discounts? It's Genius.
							</Typography>
							<Typography
								variant="body1"
								sx={{ fontSize: "24px" }}
							>
								Get rewarded for your travels – unlock instant
								savings of 10% or more with a free Booking.com
								account
							</Typography>
						</Stack>
						<HeaderButton variant="contained">
							Sign in/Register
						</HeaderButton>
						<HeaderSearch direction="row">
							<HeaderSearchItem direction="row">
								<HotelIcon />
								<InputBase
									placeholder="Where are you going?"
									sx={{ color: "#333" }}
									value={destination}
									onChange={(e) =>
										setDestination(e.target.value)
									}
								/>
							</HeaderSearchItem>
							<HeaderSearchItem
								direction="row"
								sx={{ cursor: "pointer" }}
								ref={dateRef}
							>
								<CalendarMonthIcon />
								<Typography
									variant="subtitle1"
									component="span"
									onClick={() => setOpenDate(true)}
								>
									{`${format(
										date[0].startDate,
										"MM/dd/yyyy"
									)} to ${format(
										date[0].endDate,
										"MM/dd/yyyy"
									)}`}
								</Typography>
								{openDate && (
									<DateRange
										editableDateInputs={true}
										onChange={(item) =>
											setDate([item.selection])
										}
										moveRangeOnFirstSelection={false}
										ranges={date}
										className={styles.Date}
										minDate={new Date()}
									/>
								)}
							</HeaderSearchItem>
							<HeaderSearchItem
								direction="row"
								sx={{ cursor: "pointer" }}
								onClick={handleOpenOptions}
							>
								<BoyIcon sx={{ fontSize: "30px" }} />
								<Typography
									variant="subtitle1"
									component="span"
								>
									{`${options.adults} ${
										options.adults > 1 ? "adults" : "adult"
									} ${options.children} ${
										options.children > 1
											? "children"
											: "child"
									} ${options.rooms} ${
										options.rooms > 1 ? "rooms" : "room"
									}`}
								</Typography>
							</HeaderSearchItem>
							<HeaderSearchItem direction="row">
								<HeaderButton
									sx={{ width: "100px" }}
									variant="contained"
									onClick={handleSearch}
								>
									Search
								</HeaderButton>
							</HeaderSearchItem>
						</HeaderSearch>
						<Menu
							id="basic-menu"
							anchorEl={anchorEl}
							open={!!anchorEl}
							onClose={() => setAnchorEl(null)}
							MenuListProps={{
								"aria-labelledby": "basic-button",
							}}
						>
							<ListOption gap={3}>
								<ListOptionItem>
									<Typography>Adults</Typography>
									<ListOptionItemAction>
										<Button
											variant="contained"
											onClick={() =>
												handleOptionsChange(
													"adults",
													TYPE.DECREASE
												)
											}
											disabled={options.adults <= 1}
										>
											-
										</Button>
										<Box
											sx={{
												width: "20px",
												textAlign: "center",
											}}
										>
											{options.adults}
										</Box>
										<Button
											variant="contained"
											onClick={() =>
												handleOptionsChange(
													"adults",
													TYPE.INCREASE
												)
											}
										>
											+
										</Button>
									</ListOptionItemAction>
								</ListOptionItem>
								<ListOptionItem>
									<Typography>Children</Typography>
									<ListOptionItemAction>
										<Button
											variant="contained"
											onClick={() =>
												handleOptionsChange(
													"children",
													TYPE.DECREASE
												)
											}
											disabled={options.children < 1}
										>
											-
										</Button>
										<Box
											sx={{
												width: "20px",
												textAlign: "center",
											}}
										>
											{options.children}
										</Box>
										<Button
											variant="contained"
											onClick={() =>
												handleOptionsChange(
													"children",
													TYPE.INCREASE
												)
											}
										>
											+
										</Button>
									</ListOptionItemAction>
								</ListOptionItem>
								<ListOptionItem>
									<Typography>Rooms</Typography>
									<ListOptionItemAction>
										<Button
											variant="contained"
											onClick={() =>
												handleOptionsChange(
													"rooms",
													TYPE.DECREASE
												)
											}
											disabled={options.rooms <= 1}
										>
											-
										</Button>
										<Box
											sx={{
												width: "20px",
												textAlign: "center",
											}}
										>
											{options.rooms}
										</Box>
										<Button
											variant="contained"
											onClick={() =>
												handleOptionsChange(
													"rooms",
													TYPE.INCREASE
												)
											}
										>
											+
										</Button>
									</ListOptionItemAction>
								</ListOptionItem>
							</ListOption>
						</Menu>
					</>
				)}
			</HeaderWrapper>
		</HeaderContainer>
	);
};

export default Header;
