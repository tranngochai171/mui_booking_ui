import { Button, Stack, styled, Typography } from "@mui/material";

const SearchItemContainer = styled(Stack)({
	flexDirection: "row",
	justifyContent: "space-between",
	border: "1px solid #555",
	borderRadius: "10px",
	padding: "10px",
	width: "100%",
	gap: "20px",
});
const SearchItemImg = styled("img")({
	height: "200px",
	width: "200px",
	objectFit: "cover",
});
const SearchItemDescription = styled(Stack)({
	gap: "10px",
	justifyContent: "space-between",
	flex: 2,
});

const SITitle = styled(Typography)({
	fontSize: "20px",
	color: "#0071c2",
});
const SIDistance = styled(Typography)({
	fontSize: "12px",
});
const SITaxiOp = styled(Typography)({
	fontSize: "12px",
	backgroundColor: "#008009",
	color: "#fff",
	// alignSelf: "flex-start",
	width: "max-content",
	padding: "3px",
	borderRadius: "5px",
});
const SISubtitle = styled(Typography)({
	fontSize: "12px",
	fontWeight: "bold",
});
const SIFeatures = styled(Typography)({
	fontSize: "12px",
	fontWeight: "bold",
});
const SICancelOp = styled(Typography)({
	fontSize: "12px",
	fontWeight: "bold",
	color: "#008009",
});
const SICancelOpSubtitle = styled(Typography)({
	fontSize: "12px",
	color: "#008009",
});
const SearchItemDetails = styled(Stack)({
	flex: 1,
	justifyContent: "space-between",
});
const SearchItemDetailsRating = styled(Stack)({
	flexDirection: "row",
	justifyContent: "space-between",
});
const SearchItemDetailsRatingText = styled(Typography)({
	fontSize: "20px",
	fontWeight: "bold",
});
const SearchItemDetailsRatingButton = styled(Button)({
	padding: "2px",
	minWidth: "30px",
	fontWeight: "bold",
});
const SearchItemDetailsText = styled(Stack)({
	textAlign: "right",
	gap: "5px",
});
const SearchItemDetailsTextPrice = styled(Typography)({
	fontSize: "24px",
});
const SearchItemDetailsTextTax = styled(Typography)({
	fontSize: "12px",
	color: "#555",
});
const SearchItemDetailsCheckButton = styled(Button)({
	textTransform: "unset",
	fontWeight: "bold",
});
const SearchItem = () => {
	return (
		<SearchItemContainer>
			<SearchItemImg src="https://cf.bstatic.com/xdata/images/hotel/square600/261707778.webp?k=fa6b6128468ec15e81f7d076b6f2473fa3a80c255582f155cae35f9edbffdd78&o=&s=1" />
			<SearchItemDescription>
				<SITitle variant="h1">Tower Street Apartment</SITitle>
				<SIDistance component="span">500m from center</SIDistance>
				<SITaxiOp component="span">Free airport taxi</SITaxiOp>
				<SISubtitle component="span">
					Studio Apartment with Air conditioning
				</SISubtitle>
				<SIFeatures component="span">Entire studio</SIFeatures>
				<SICancelOp component="span">Free cancellation</SICancelOp>
				<SICancelOpSubtitle component="span">
					You can cancel later, so lock in this great price today!
				</SICancelOpSubtitle>
			</SearchItemDescription>
			<SearchItemDetails>
				<SearchItemDetailsRating>
					<SearchItemDetailsRatingText>
						Excellent
					</SearchItemDetailsRatingText>
					<SearchItemDetailsRatingButton variant="contained">
						8.9
					</SearchItemDetailsRatingButton>
				</SearchItemDetailsRating>
				<SearchItemDetailsText>
					<SearchItemDetailsTextPrice>
						$123
					</SearchItemDetailsTextPrice>
					<SearchItemDetailsTextTax>
						Includes taxes and fees
					</SearchItemDetailsTextTax>
					<SearchItemDetailsCheckButton variant="contained">
						See availability
					</SearchItemDetailsCheckButton>
				</SearchItemDetailsText>
			</SearchItemDetails>
		</SearchItemContainer>
	);
};

export default SearchItem;
