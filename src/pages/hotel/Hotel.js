import {
	styled,
	Stack,
	Box,
	Typography,
	Button,
	IconButton,
} from "@mui/material";
import { useState } from "react";
import Footer from "../../components/Footer";
import Header from "../../components/Header";
import MailList from "../../components/MailList";
import Navbar from "../../components/Navbar";
import LocationOnIcon from "@mui/icons-material/LocationOn";
import CloseIcon from "@mui/icons-material/Close";
import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIosNew";
import ArrowForwardIosIcon from "@mui/icons-material/ArrowForwardIos";

const HotelContainer = styled(Stack)({
	flexDirection: "column",
	alignItems: "center",
	gap: "30px",
	marginTop: "20px",
});

const HotelWrapper = styled(Stack)({
	width: "100%",
	maxWidth: "1024px",
	gap: "10px",
	position: "relative",
});

const HotelButton = styled(Button)({
	textTransform: "unset",
	fontWeight: "bold",
});

const HotelBookNowButton = styled(Button)({
	position: "absolute",
	top: "10px",
	right: 0,
});

const HotelTitle = styled(Typography)({
	fontSize: "24px",
});
const HotelAddress = styled(Stack)({
	fontSize: "12px",
	flexDirection: "row",
	alignItems: "center",
	gap: "10px",
});
const HotelDistance = styled(Typography)({
	color: "#0071c2",
	fontWeight: "500",
});
const HotelPriceHighlight = styled(Typography)({
	color: "#008009",
	fontWeight: "500",
});
const HotelImages = styled(Stack)({
	flexDirection: "row",
	flexWrap: "wrap",
	justifyContent: "space-between",
});
const HotelImageWrapper = styled(Box)({
	width: "33%",
});
const HotelImage = styled("img")({
	width: "100%",
	objectFit: "cover",
	cursor: "pointer",
});
const HotelDetails = styled(Stack)({
	flexDirection: "row",
	justifyContent: "space-between",
	gap: "20px",
});
const HotelDetailsText = styled(Stack)({
	flex: 3,
	gap: "20px",
});

const HotelDetailsTextTitle = styled(Typography)({
	fontSize: "18px",
	color: "#555",
});

const HotelDetailsTextDesc = styled(Typography)({
	fontSize: "14px",
});
const HotelDetailsPrice = styled(Stack)({
	flex: 1,
	backgroundColor: "#ebf3ff",
	padding: "20px",
	gap: "20px",
});

const HotelDetailsPriceText = styled(Typography)({
	fontSize: "24px",
	fontWeight: "300",
});

const Slider = styled(Stack)({
	position: "fixed",
	top: 0,
	left: 0,
	width: "100vw",
	height: "100vh",
	backgroundColor: "rgba(0, 0, 0, 0.613)",
	zIndex: 999,
	alignItems: "center",
});
const SliderWrapper = styled(Stack)({
	width: "100%",
	height: "100%",
	justifyContent: "center",
	alignItems: "center",
	flexDirection: "row",
	gap: "30px",
});

const CloseButton = styled(IconButton)({
	position: "absolute",
	top: "20px",
	right: "40px",
	color: "lightgray",
});

const SliderImage = styled("img")({
	width: "80%",
	height: "80%",
});

const LeftButton = styled(IconButton)({
	color: "lightgray",
});
const RightButton = styled(IconButton)({
	color: "lightgray",
});

const photos = [
	{
		src: "https://cf.bstatic.com/xdata/images/hotel/max1280x900/261707778.jpg?k=56ba0babbcbbfeb3d3e911728831dcbc390ed2cb16c51d88159f82bf751d04c6&o=&hp=1",
	},
	{
		src: "https://cf.bstatic.com/xdata/images/hotel/max1280x900/261707367.jpg?k=cbacfdeb8404af56a1a94812575d96f6b80f6740fd491d02c6fc3912a16d8757&o=&hp=1",
	},
	{
		src: "https://cf.bstatic.com/xdata/images/hotel/max1280x900/261708745.jpg?k=1aae4678d645c63e0d90cdae8127b15f1e3232d4739bdf387a6578dc3b14bdfd&o=&hp=1",
	},
	{
		src: "https://cf.bstatic.com/xdata/images/hotel/max1280x900/261707776.jpg?k=054bb3e27c9e58d3bb1110349eb5e6e24dacd53fbb0316b9e2519b2bf3c520ae&o=&hp=1",
	},
	{
		src: "https://cf.bstatic.com/xdata/images/hotel/max1280x900/261708693.jpg?k=ea210b4fa329fe302eab55dd9818c0571afba2abd2225ca3a36457f9afa74e94&o=&hp=1",
	},
	{
		src: "https://cf.bstatic.com/xdata/images/hotel/max1280x900/261707389.jpg?k=52156673f9eb6d5d99d3eed9386491a0465ce6f3b995f005ac71abc192dd5827&o=&hp=1",
	},
];
const Hotel = () => {
	const [openSlider, setOpenSlider] = useState(false);
	const [currentSlider, setCurrentSlider] = useState(0);
	const handleOpenSlider = (index) => {
		setCurrentSlider(index);
		setOpenSlider(true);
	};
	const handleChangeSliderImg = (type) => {
		setCurrentSlider(
			type === "l"
				? currentSlider === 0
					? photos.length - 1
					: currentSlider - 1
				: currentSlider === photos.length - 1
				? 0
				: currentSlider + 1
		);
	};
	return (
		<>
			{openSlider && (
				<Slider>
					<CloseButton onClick={() => setOpenSlider(false)}>
						<CloseIcon />
					</CloseButton>
					<SliderWrapper>
						<LeftButton onClick={() => handleChangeSliderImg("l")}>
							<ArrowBackIosNewIcon />
						</LeftButton>
						<SliderImage src={photos[currentSlider].src} />
						<RightButton onClick={() => handleChangeSliderImg("r")}>
							<ArrowForwardIosIcon />
						</RightButton>
					</SliderWrapper>
				</Slider>
			)}
			<Navbar />
			<Header showOptions={false} />
			<HotelContainer>
				<HotelWrapper>
					<HotelBookNowButton variant="contained">
						Reserve or Book now!
					</HotelBookNowButton>
					<HotelTitle>Grand Hotel</HotelTitle>
					<HotelAddress>
						<LocationOnIcon />
						<Typography>Elton St 125 New york</Typography>
					</HotelAddress>
					<HotelDistance>
						Excellent location - 500m from center
					</HotelDistance>
					<HotelPriceHighlight>
						Book a stay over $114 at this property and get a free
						airport taxi
					</HotelPriceHighlight>
					<HotelImages>
						{photos.map((photo, index) => (
							<HotelImageWrapper
								key={index}
								onClick={() => handleOpenSlider(index)}
							>
								<HotelImage src={photo.src} />
							</HotelImageWrapper>
						))}
					</HotelImages>
					<HotelDetails>
						<HotelDetailsText>
							<h1>Stay in the heart of City</h1>
							<HotelDetailsTextDesc>
								Located a 5-minute walk from St. Florian's Gate
								in Krakow, Tower Street Apartments has
								accommodations with air conditioning and free
								WiFi. The units come with hardwood floors and
								feature a fully equipped kitchenette with a
								microwave, a flat-screen TV, and a private
								bathroom with shower and a hairdryer. A fridge
								is also offered, as well as an electric tea pot
								and a coffee machine. Popular points of interest
								near the apartment include Cloth Hall, Main
								Market Square and Town Hall Tower. The nearest
								airport is John Paul II International
								Kraków–Balice, 16.1 km from Tower Street
								Apartments, and the property offers a paid
								airport shuttle service.
							</HotelDetailsTextDesc>
						</HotelDetailsText>
						<HotelDetailsPrice>
							<HotelDetailsTextTitle>
								Perfect for a 9-night stay!
							</HotelDetailsTextTitle>
							<span>
								Located in the real heart of Krakow, this
								property has an excellent location score of 9.8!
							</span>
							<HotelDetailsPriceText>
								<b>$945</b> (9 nights)
							</HotelDetailsPriceText>
							<HotelButton variant="contained">
								Reserve or Book Now!
							</HotelButton>
						</HotelDetailsPrice>
					</HotelDetails>
				</HotelWrapper>
				<MailList />
				<Footer />
			</HotelContainer>
		</>
	);
};

export default Hotel;
