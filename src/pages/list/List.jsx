import {
	styled,
	Box,
	Stack,
	Typography,
	InputBase,
	Button,
} from "@mui/material";
import { useLocation } from "react-router-dom";
import { useRef, useState } from "react";
import { useImmer } from "use-immer";
import { format } from "date-fns";
import { DateRange } from "react-date-range";
import Navbar from "../../components/Navbar";
import Header from "../../components/Header";
import useOnClickOutside from "../../hooks/useOnClickOutside";
import SearchItem from "../../components/SearchItem";

const ListContainer = styled(Stack)({
	flexDirection: "row",
	justifyContent: "center",
	marginTop: "20px",
});
const ListWrapper = styled(Stack)({
	width: "100%",
	maxWidth: "1024px",
	flexDirection: "row",
	gap: "20px",
});
const ListSearch = styled(Stack)({
	flex: 1,
	backgroundColor: "#febb02",
	color: "#555",
	padding: "10px",
	borderRadius: "10px",
	position: "sticky",
	top: "10px",
	gap: "10px",
	// alignSelf: "flex-start",
	height: "max-content",
});

const ListSearchTitle = styled(Typography)({
	fontSize: "24px",
	fontWeight: "bold",
});

const ListSearchItem = styled(Stack)({});

const ListSearchItemInput = styled(InputBase)({
	backgroundColor: "#fff",
	padding: "0 4px",
});

const DateSelection = styled(Typography)({
	backgroundColor: "#fff",
	padding: "5px",
	display: "flex",
	alignItems: "center",
});

const ListResult = styled(Stack)({
	flex: 3,
	gap: "30px",
});

const ListSearchOptionItem = styled(Stack)({
	gap: "5px",
	padding: "10px",
});

const SearchButton = styled(Button)({});

const List = () => {
	const location = useLocation();
	const dateRef = useRef();
	const [openDate, setOpenDate] = useState(false);
	useOnClickOutside(dateRef, () => setOpenDate(false));
	const [date, setDate] = useState(
		location?.state?.date ?? [
			{
				startDate: new Date(),
				endDate: new Date(),
				key: "selection",
			},
		]
	);
	const [destination, setDestination] = useState(
		location?.state?.destination ?? ""
	);
	const [options, setOptions] = useImmer({
		adults: 1,
		children: 0,
		rooms: 1,
	});
	return (
		<>
			<Navbar />
			<Header showOptions={false} />
			<ListContainer>
				<ListWrapper>
					<ListSearch>
						<ListSearchTitle>Search</ListSearchTitle>
						<ListSearchItem>
							<Typography variant="subtitle1">
								Destination
							</Typography>
							<ListSearchItemInput
								value={destination}
								onChange={(e) => setDestination(e.target.value)}
							/>
						</ListSearchItem>
						<ListSearchItem>
							<Typography variant="subtitle1">
								Check-in Date
							</Typography>
							<DateSelection
								ref={dateRef}
								onClick={() => setOpenDate(true)}
							>{`${format(
								date[0].startDate,
								"MM/dd/yyyy"
							)} to ${format(
								date[0].endDate,
								"MM/dd/yyyy"
							)}`}</DateSelection>
							{openDate && (
								<DateRange
									editableDateInputs={true}
									onChange={(item) =>
										setDate([item.selection])
									}
									moveRangeOnFirstSelection={false}
									ranges={date}
									minDate={new Date()}
								/>
							)}
						</ListSearchItem>
						<ListSearchItem>
							<Typography variant="subtitle1">Options</Typography>
							<ListSearchOptionItem>
								<Stack
									direction="row"
									sx={{
										justifyContent: "space-between",
										alignItems: "center",
									}}
								>
									<Typography sx={{ flex: 4 }}>
										Adult
									</Typography>
									<ListSearchItemInput
										sx={{ flex: 1 }}
										min={1}
										type="number"
									/>
								</Stack>
								<Stack
									direction="row"
									sx={{
										justifyContent: "space-between",
										alignItems: "center",
									}}
								>
									<Typography sx={{ flex: 4 }}>
										children
									</Typography>
									<ListSearchItemInput
										sx={{ flex: 1 }}
										min={1}
										type="number"
									/>
								</Stack>
								<Stack
									direction="row"
									sx={{
										justifyContent: "space-between",
										alignItems: "center",
									}}
								>
									<Typography sx={{ flex: 4 }}>
										Room
									</Typography>
									<ListSearchItemInput
										sx={{ flex: 1 }}
										min={1}
										type="number"
									/>
								</Stack>
							</ListSearchOptionItem>
						</ListSearchItem>
						<SearchButton variant="contained">Search</SearchButton>
					</ListSearch>
					<ListResult>
						<SearchItem />
						<SearchItem />
						<SearchItem />
						<SearchItem />
						<SearchItem />
					</ListResult>
				</ListWrapper>
			</ListContainer>
		</>
	);
};

export default List;
