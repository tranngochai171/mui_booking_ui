import { Stack, styled, Typography } from "@mui/material";
import Featured from "../../components/Featured";
import FeaturedProperties from "../../components/FeaturedProperties";
import Footer from "../../components/Footer";
import Header from "../../components/Header";
import MailList from "../../components/MailList";
import Navbar from "../../components/Navbar";
import PropertyList from "../../components/PropertyList";

const HomeContainer = styled(Stack)({
	flexDirection: "column",
	alignItems: "center",
	gap: "30px",
	marginTop: "50px",
});

const HomeTitle = styled(Typography)({
	width: "1024px",
	fontSize: "20px",
	fontWeight: "bold",
});

const Home = () => {
	return (
		<>
			<Navbar />
			<Header />
			<HomeContainer>
				<Featured />
				<HomeTitle>Browse by property type</HomeTitle>
				<PropertyList />
				<HomeTitle>Homes guests love</HomeTitle>
				<FeaturedProperties />
				<MailList />
				<Footer />
			</HomeContainer>
		</>
	);
};

export default Home;
